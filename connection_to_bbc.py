import time
from selenium import webdriver
from argparse import ArgumentParser
import subprocess



parser = ArgumentParser()
parser.add_argument("-ip", "--ipaddress", dest="ip_address",
                    help="write report to FILE", default="rr.beebs.host")
args = parser.parse_args()
PROXY = args.ip_address + ":80" # IP:PORT or HOST:PORT


chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument('--proxy-server=%s' % PROXY)
chrome_options.add_argument('--headless')
chrome_options.add_argument("--window-size=1920x1080")
chrome_options.add_argument('--no-sandbox')

chrome = webdriver.Chrome('/usr/local/bin/chromedriver',chrome_options=chrome_options)

def bbc_run1():
    chrome.get("https://www.bbc.co.uk/iplayer/episode/b00nxn31/miranda-series-1-1-date")
    if "BBC" in chrome.title:
        if chrome.find_elements_by_css_selector('.banner--outside-uk-visible'):
            print('[FAILED]')
        else:
            print('[PASSED]')
    else:
        print("bad page")
bbc_run1();

chrome.quit();



# Your Account Sid and Auth Token from twilio.com/user/account
#account_sid = "ACac76928263c237b2fcfa680bc20b4e4f"
#auth_token = "475d857308136a8baff04567cc1e472d"
#client = Client(account_sid, auth_token)
