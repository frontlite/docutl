#!/bin/bash

LB_ID="9be75047-63ee-461a-a03f-6ddd81a822eb"



function spin_up_droplet() {
  echo "Starting new droplet..."
  OUTPUT=$(doctl compute droplet create ${hourandminute} --size 1gb --image 30454842 --wait --region lon1 --ssh-keys 39:b5:77:a0:ce:f8:b6:88:23:98:d6:fc:62:0f:04:77)
  IDtemp=$(echo ${OUTPUT} | tail -1 | grep -E -o '[0-9]{8}')
  ID=${IDtemp}
  echo "ID: " ${ID}
}

function grab_ip() {
  echo "Checking out IP..."
  RAWIPADDRESS=$(doctl compute droplet get ${ID})
  A=$(echo ${RAWIPADDRESS} | grep -E -o '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}')
  IP=${A}
  echo "IP is " ${A}
}

function connection_to_beebs() {
  echo "Connecting to the BBC..."
  OUTCOME=$(python connection_to_bbc.py --ip $1);
  echo "Server has" ${OUTCOME}
}

function update_load_balancer() {
  echo "Updating Load Balancer..."
  doctl compute load-balancer add-droplets ${LB_ID} --droplet-ids ${ID}
  echo "Updated!"
}

function start() {
  spin_up_droplet;
  echo "Waiting for IP..."
  grab_ip ${ID};
  connection_to_beebs ${IP};
}

function find_oldest_server(){
  oldserver=$(doctl compute droplet list | head -2 | tail -1 | grep -E -o '[0-9]{8}')
  echo ${oldserver} " is the oldest server"
}

function deleting_server() {
  echo "Deleting server..."
  doctl compute droplet delete -f $1;
  echo "Server Deleted."
}

function cleaning_server() {
  find_oldest_server;
  IPTODEL=${oldserver};
  echo "Deleting server...";
  doctl compute droplet delete -f ${IPTODEL};
  echo "Server Deleted.";
}

function main() {
  hourandminute=$(date +"%d%H%M")
  start
  if [ ${OUTCOME} = '[PASSED]' ]; then
    update_load_balancer ${ID};
    cleaning_server;
  else
    if [ ! -z "$ID" ]; then
      deleting_server $ID;
      main;
    fi
  fi
}

main
